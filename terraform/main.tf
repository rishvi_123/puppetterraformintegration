provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "agent" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "terrakey"
  vpc_security_group_ids = [aws_security_group.main.id]

  tags = {
    Name = "agent"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("terrakey.pem")
    host        = self.public_ip
    #port = 22
    timeout     = "3m"
  }

  provisioner "remote-exec" {
    inline = [
      
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"192.168.10.150 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test",
      #"touch hello.txt",
      #"echo helloworld >> hello.txt",


    ]
  }
  

}
resource "aws_security_group" "main" {
  egress = [
    {
      cidr_blocks      = [ "0.0.0.0/0", ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    }
  ]
 ingress                = [
   {
     cidr_blocks      = [ "0.0.0.0/0", ]
     description      = ""
     from_port        = 22
     ipv6_cidr_blocks = []
     prefix_list_ids  = []
     protocol         = "tcp"
     security_groups  = []
     self             = false
     to_port          = 22
  }
  ]
}
resource "aws_key_pair" "deployer" {
  key_name   = "aws_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDq1+6WPeRhUOsKXlk4UrJKbuBymsCD7R4Tgh3uezDSga6Stnpxk4A2DMbvb34J54raCtCM4mj6yANvNRVdAD3sJ54xLqathS6ktylsEmWwVxDBQPW0EjfPtTewUXb4iwV09DtO5JdEkp/NoO2wTpxc4W5yx5p4rhFGSTACLj7OLCGMgnCOFR2/gP04urZeT4H1cr/+BTyUnq7gwsJCXjrtZybRZpts7DM+VEy3ROUpz/3NrtVrqlVOpRVvCwZw2XgK7cGnfzMmcnoHkNwqivC4pgpMj+K9muz5y9YtYWCT8LcQuIMBVE175drdjuDz0fz6hFVsveqwxa6R6o8/qaG9 rishv@LAPTOP-OQ6E6U0G"
}